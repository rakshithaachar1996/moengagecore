// swift-tools-version:5.3
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "MoEngageCore",
    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        .library(
            name: "MoEngageCore",
            targets: ["MoEngageCore"]),
    ],
    dependencies: [
        // Dependencies declare other packages that this package depends on.
        // .package(url: /* package url */, from: "1.0.0"),
    ],
    targets: [
        .binaryTarget(name: "MoEngageCore", url: "https://cordovasampl.web.app/MoEngageCore.xcframework.zip", checksum: "69f6fa562ab44c2aef55a4784d5ff1149d0a80bafc93200d8f12b43a7ed65265")
    ]
)
